using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnderTest.FeatureTransform.Markdown.Model;
using UnderTest.FeatureTransform.Options;

namespace UnderTest.FeatureTransform.Markdown
{
  public class MarkdownReport
  {
    public MarkdownReport(MarkdownOptions optionsP)
    {
      _options = optionsP;
    }

    private readonly MarkdownOptions _options;

    private string _storeReportPath;
    private UnderTestReport _underTestReport;


    public void WriteReportFiles()
    {
      _storeReportPath = Path.Combine(_options.WorkingDirectory, "MarkdownReport");

      CheckDir(_storeReportPath);

      _underTestReport = LoadJsonReport(_options.ReportFile);

      using (var write = new StreamWriter(Path.Combine(_storeReportPath, _options.OutputFilename)))
      {
        write.WriteLine($"# Project: {_underTestReport.ProjectName}\n");
        write.WriteLine($"## Version: {_underTestReport.ProjectVersion}\n");
        write.WriteLine($"## Overall Result: {_underTestReport.OverallResult.Name}\n");
        write.WriteLine("| Start Time | End Time | Duration |\r\n |---|---|---|");
        write.WriteLine(
          $"| {_underTestReport.ExecutionTime.StartTime.ToString()} | {_underTestReport.ExecutionTime.EndTime.ToString()} | {_underTestReport.ExecutionTime.Duration.ToString()} |");

        write.WriteLine();
        write.WriteLine("| Scenarios | \r\n |---|");
        write.WriteLine($"|Passed: {_underTestReport.Scenarios.Passed}|");
        write.WriteLine($"|Failed: {_underTestReport.Scenarios.Failed}|");
        write.WriteLine($"|Inconclusive: {_underTestReport.Scenarios.Inconclusive}|");
        write.WriteLine($"|Skipped: {_underTestReport.Scenarios.Skipped}|");
        write.WriteLine($"|Wip: {_underTestReport.Scenarios.Wip}|");
        write.WriteLine($"|Total: {_underTestReport.Scenarios.Total}|\n");

        write.WriteLine("### Features");
        write.Write(BuildFeatureTable());
      }

    }

    private StringBuilder BuildFeatureTable()
    {
      var stringBuilder = new StringBuilder();
      stringBuilder.AppendLine("| FileName | Results |");
      stringBuilder.AppendLine("|---|---|");
      foreach (var feature in _underTestReport.Features)
      {
        stringBuilder.AppendLine(
          $"|[{feature.Name}](.\\{feature.FeatureFileName}.md)|");
        BuildFeatureReport(feature);
      }
      return  stringBuilder;
    }

    private void BuildFeatureReport(Feature feature)
    {
      var relativePath = Path.GetDirectoryName((string) feature.FeatureFileName);
      CheckDir(Path.Combine(_storeReportPath, relativePath));

      //TODO paths to files need to be fixed
      using (var write = new StreamWriter(Path.Combine(_storeReportPath, $"{feature.FeatureFileName}.md")))
      {
        write.WriteLine($"# {feature.Name}\n");
        write.WriteLine(">>>");
        write.WriteLine($">{feature.FeatureDescription}");
        write.WriteLine(">>>\n");
        foreach (var run in feature.StrategyRuns)
        {
          write.WriteLine("| Start Time | End Time | Duration |\r\n |---|---|---|");
          write.WriteLine($"| {run.StartTime.ToString()} | {run.EndTime.ToString()} | {run.Duration.ToString()} |");
          foreach (var testResult in run.ScenarioTestResults)
          {
            write.WriteLine($"### {testResult.Keyword} {testResult.ScenarioName} : {testResult.Result.Name}\n");

            write.WriteLine($"{testResult.Description}\n");

            write.WriteLine("|Keyword|Step|Result|Arguments|Messages|Duration|");
            write.WriteLine("|---|---|---|---|---|---|---|");

            foreach (var stepResults in testResult.StepResults)
            {
              write.Write($"|{stepResults.Keyword}|`{stepResults.Step}`|{stepResults.ScenarioStepResultType.Name}|");

              write.Write(stepResults.StepParameters != null ? $"{string.Join("; ", stepResults.StepParameters)}|" : "|");

              write.Write($"{string.Join("; ", stepResults.LogMessages.Select(x => $"```{x.Type} : {x.Message}```"))}|");

              write.Write($"{stepResults.Duration.ToString()}|\n");
            }
          }
        }
      }
    }

    private UnderTestReport LoadJsonReport(string pathP)
    {
      using (var reader = new StreamReader(pathP))
      {
        var fileText = reader.ReadToEnd();
        return JsonConvert.DeserializeObject<UnderTestReport>(fileText);
      }
    }

    private void CheckDir(string pathP)
    {
      if (!Directory.Exists(pathP))
      {
        Directory.CreateDirectory(pathP);
      }
    }
  }
}
