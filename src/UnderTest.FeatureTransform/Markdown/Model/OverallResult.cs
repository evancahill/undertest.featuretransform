using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class OverallResult
  {
    [JsonProperty("ID")]
    public int Id { get; set; }

    [JsonProperty("Name")]
    public string Name { get; set; }
  }
}
