namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class StepNotice
  {
    public string NoticeCode { get; set; }

    public string Message { get; set; }
  }
}
