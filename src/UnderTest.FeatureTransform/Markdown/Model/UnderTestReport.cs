using System.Collections.Generic;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class UnderTestReport
  {
    [JsonProperty("ProjectName")]
    public string ProjectName { get; set; }

    [JsonProperty("ProjectVersion")]
    public string ProjectVersion { get; set; }

    [JsonProperty("Features")]
    public IList<Feature> Features { get; set; }

    [JsonProperty("Scenarios")]
    public Scenarios Scenarios { get; set; }

    [JsonProperty("UnderTestVersion")]
    public string UnderTestVersion { get; set; }

    [JsonProperty("OverallResult")]
    public OverallResult OverallResult { get; set; }

    [JsonProperty("ExecutionTime")]
    public ExecutionTime ExecutionTime { get; set; }
  }
}
