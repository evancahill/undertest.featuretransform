using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class LogMessage
  {
    [JsonProperty("Type")]
    public string Type { get; set; }

    [JsonProperty("Message")]
    public string Message { get; set; }

    [JsonProperty("StackTrace")]
    public object StackTrace { get; set; }
  }
}
