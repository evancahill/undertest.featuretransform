using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class Scenarios
  {
    [JsonProperty("Passed")]
    public long Passed { get; set; }

    [JsonProperty("Failed")]
    public long Failed { get; set; }

    [JsonProperty("Inconclusive")]
    public long Inconclusive { get; set; }

    [JsonProperty("Skipped")]
    public long Skipped { get; set; }

    [JsonProperty("Wip")]
    public long Wip { get; set; }

    [JsonProperty("Total")]
    public long Total { get; set; }
  }
}
