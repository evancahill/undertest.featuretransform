using System;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class ExecutionTime
  {
    [JsonProperty("StartTime")]
    public DateTimeOffset StartTime { get; set; }

    [JsonProperty("EndTime")]
    public DateTimeOffset EndTime { get; set; }

    [JsonProperty("Duration")]
    public TimeSpan Duration { get; set; }
  }
}
