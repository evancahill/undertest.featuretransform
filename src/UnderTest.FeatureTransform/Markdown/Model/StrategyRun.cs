using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UnderTest.FeatureTransform.Markdown.Model
{
  public class StrategyRun
  {
    [JsonProperty("StrategyName")]
    public string StrategyName { get; set; }

    [JsonProperty("Duration")]
    public TimeSpan Duration { get; set; }

    [JsonProperty("EndTime")]
    public DateTimeOffset EndTime { get; set; }

    [JsonProperty("ScenarioTestResults")]
    public IList<ScenarioTestResult> ScenarioTestResults { get; set; }

    [JsonProperty("StartTime")]
    public DateTimeOffset StartTime { get; set; }
  }
}
