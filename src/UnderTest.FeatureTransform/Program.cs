using System;
using System.IO;
using CommandLine;
using UnderTest.FeatureTransform.Markdown;
using UnderTest.FeatureTransform.Options;
using UnderTest.FeatureTransform.Summary;
using UnderTest.FeatureTransform.WorkBooks;
using static UnderTest.FeatureTransform.IO.FileCollector;

namespace UnderTest.FeatureTransform
{
  class Program
  {
    static int Main(string[] args)
    {
      // todo: handle duplicate feature names
      var workingDirectory = Directory.GetCurrentDirectory();
      try
      {
        Parser.Default.ParseArguments<ExcelOptions, SummaryOptions>(args)
          .WithParsed<ExcelOptions>(o =>
            {
              o.DefaultWorkingDirectory(workingDirectory);
              var files = CollectFilesFromGlobs(o.WorkingDirectory, o.FeaturePaths);

              new QaWorkBook(o, files).Process();
            })
          .WithParsed<SummaryOptions>(o =>
            {
              o.DefaultWorkingDirectory(workingDirectory);
              var files = CollectFilesFromGlobs(o.WorkingDirectory, o.FeaturePaths);
              new FeatureSummary(o, files).CreateFeatureSummary();
            }).WithParsed<MarkdownOptions>(o =>
          {
            o.DefaultWorkingDirectory(workingDirectory);
            new MarkdownReport(o).WriteReportFiles();
          });
      }
      catch (Exception e)
      {
        Console.WriteLine($"General failure: {e.Message}");
        return (int)ExitCodes.GeneralFailure;
      }

      return (int)ExitCodes.Success;
    }
  }
}
