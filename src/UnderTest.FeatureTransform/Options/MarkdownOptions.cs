using System.Collections;
using System.Collections.Generic;
using CommandLine;

namespace UnderTest.FeatureTransform.Options
{
  [Verb("markdown", HelpText = "Create Markdown report for GitLab/Github")]
  public class MarkdownOptions : IStandardVerbOptions
  {
    [Value(0, MetaName = "filepaths", Required = false, Default = new[] {"**/*.feature"},
      HelpText = "Input filepath(s).  Supports globs, absolute and relative paths.")]
    public IEnumerable<string> FeaturePaths { get; set; }

    [Option('r', "report", Required = false, Default = @".\bin\Debug\netcoreapp2.2\reports\undertest-result.json",
      HelpText = "Input report file.  Supports Undertest reports")]
    public string ReportFile { get; set; }

    [Option('f', "filename", Default = "TestMarkdownReport.md", Required = false, HelpText = "Output filename.")]
    public string OutputFilename { get; set; }

    [Option('w', "workingDirectory", Default = null, Required = false, HelpText = "Folder to execute this command in.  Defaults to the current directory.")]
    public string WorkingDirectory { get; set; }
  }
}
