# UnderTest.FeatureTransform

A project for transforming feature files into various other formats.


Currently, FeatureTransform supports the following:

* generating feature files into excel sheets meant for manual user testing
* creatning a summary markdown page of all feature files found in a project

## Install
1. Install the global tool via `dotnet tool install -g UnderTest.FeatureTransform`

## Create Summary Page
1. Invoke `featuretransform summary` at root directory of feature file project
2. Summary page will be created listing all found feature files with their descriptions and links to each file.

## Create Excel Workbook

1. Invoke `featuretransform excel *.feature` 
2. An output Excel sheet will be output in the current directory

## Create Markdown Report

1. Invoke `featuretransform markdown -f OutputReport.md -r path\to\undertest-result.json`
2. A Markdown report will output in the directory specified in the -f filename parameter

